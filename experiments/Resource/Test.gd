tool
extends Node


export(Array, Resource) var stuff = [] setget _set_stuff

func _set_stuff(array):
	stuff = array
	for i in array.size():
		if array[i] == null:
			array[i] = TestResource.new()



# GRIMOIRE: Ars Bellica

A high fantasy, turn-based, sandbox, tactical role-playing game!

*Grimoire: Ars Bellica* was developed as part of a Ph.D. research project on
software architecture applied to economy mechanics in games. It provides a
reference implementation for both the *Unlimited Rulebook* reference
architecture and the *Rulebook* architectural pattern proposed in that
research, providing them with a proof-of-concept validation. You can find
the full thesis here:

https://www.teses.usp.br/teses/disponiveis/45/45134/tde-22122021-205515/en.php

## How to run the game

I keep the latest stable release up at
[itch.io](https://kazuo256.itch.io/grimoire-ars-bellica) but you can play
directly from the source by downloading or cloning the repository. You're going
to need the [Godot engine](https://godotengine.org/) to open the project file
(project.godot). Once open, just hit F5 to play.

## How to play the game

Grimoire: Ars Bellica uses mostly the mouse to play. Click on a position to
move there, on a spell icon to starting casting it, or on an interface button
to use it. Spells can also be cast using the keyboard keys 1-5 as shortcuts.
Use SPACE to interact with nearby things.

The objective is to find a magic crystal in the Dark Fortress. To do so, you'll
need to learn many spells and transcribe them in your grimoire. Spells can be
found on Rune Slabs — interact with them to transcribe them. If you hove no
more space left in your grimoire, you have to erase one of the spells there to
make up space. Don't worry, you can always go back to a Rune Slab and
transcribe the spell again.
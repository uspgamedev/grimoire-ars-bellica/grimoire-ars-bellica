extends Node

export var property_path: NodePath

onready var timer := 0.0

func _process(delta):
	timer += delta
	var property := get_node(property_path) as Property
	var entity := property.get_owner_entity()
	entity.offset.y = -4 + sin(timer * TAU) * 2

func _exit_tree():
	var property := get_node(property_path) as Property
	var entity := property.get_owner_entity()
	entity.offset.y = 0

class_name VFXPlayer extends Node

export var map_path := NodePath()

static func _on_node_added(node: Node):
	if node is Ability:
		SignalHelper.safe_connect(node, "performed", AbilityObserver, "_on_ability_performed")

class AbilityObserver:
	static func _on_ability_performed(ability_use: AbilityUse):
		if		ability_use.ability.is_action \
			and ability_use.target.target_offset != Tile.INVALID:
				var entity := ability_use.actor_entity
				var offset = ability_use.target.target_offset.normalized() \
						   * Tile.SIZE / 2
				entity.position += offset

class_name Ability extends Node

export var is_action := false
export var action_speed_modifier := 0

signal performed(ability)

func perform(use):
	emit_signal("performed", use)

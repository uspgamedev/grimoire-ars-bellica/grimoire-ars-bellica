tool
class_name MovementProfile extends Resource

const EMPTY		= 0
const GROUND 	= 1 << 0
const AIR 		= 1 << 1
const WATER 	= 1 << 2
const CLOUD 	= 1 << 3
const SPIRIT 	= 1 << 4
const ARCANE 	= 1 << 5

enum Operation {
	NONE,
	REPLACE,
	ADD,
	REMOVE
}

export(int, FLAGS, "Ground", "Air", "Water", "Cloud", "Spirit", "Arcane") \
var mask

export (Operation) var operation = Operation.NONE

func _init():
	resource_name = "MovementProfile"
	operation = Operation.NONE

func is_empty() -> bool:
	return mask == EMPTY

func matches(other: MovementProfile) -> bool:
	return mask & other.mask

func compose(other: MovementProfile) -> MovementProfile:
	match other.operation:
		Operation.NONE:
			return self.duplicate() as MovementProfile
		Operation.REPLACE:
			return other.duplicate() as MovementProfile
		Operation.ADD:
			var result := get_script().new() as MovementProfile
			result.mask |= other.mask
			return result
		Operation.REMOVE:
			var result := get_script().new() as MovementProfile
			result.mask ^= other.mask
			return result
	return null

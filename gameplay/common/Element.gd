class_name Element extends Object

enum TYPE {
	NONE,
	IMPACT,
	FIRE,
	SHOCK,
	LIFE,
}

const TAG = {
	TYPE.IMPACT: "impact",
	TYPE.FIRE: "fire",
	TYPE.SHOCK: "shock",
	TYPE.LIFE: "life",
}

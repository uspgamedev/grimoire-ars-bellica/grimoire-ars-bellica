extends TriggerRule

func _apply_effect(map: Map, effect: Effect):
	var self_entity := get_self_entity()
	if effect is SenseEffect:
		var sense_effect := effect as SenseEffect
		var sensed_path := sense_effect.get_subject_path()
		var sensed_entity := get_node(sensed_path) as Entity
		if		sense_effect.data.has_flag(AreaSensor.OTHER_EXITED) \
			or	sense_effect.data.has_flag(AreaSensor.SELF_EXITED):
				var webbed := StatusServer \
					.find_status_by_id(sensed_entity, "webbed")
				if		webbed != null \
					and	webbed.get_source() == self_entity:
					var remove_effect := RemoveStatusEffect.new() \
						.remove_status(webbed.get_path()) \
						.on(sensed_entity)
					effect = effect.then(remove_effect)
	elif effect is DestroyEffect:
		for entity in map.get_all_entities():
			var webbed := StatusServer.find_status_by_id(entity,
														 "webbed")
			if 		webbed != null \
				and	webbed.get_source() == self_entity:
				var remove_effect := RemoveStatusEffect.new() \
					.remove_status(webbed.get_path()) \
					.on(entity)
				effect = effect.then(remove_effect)

func find_

extends Rule

export var webbed_scene: PackedScene

func _process_effect(_map: Map, effect: Effect) -> Effect:
	var status_effect := effect as GiveStatusEffect
	if status_effect.get_status() == webbed_scene:
		return status_effect.prevent()
	else:
		return status_effect

class_name Pursuer extends AI

export var PERCEPTION_RANGE := 6

onready var walk_ability := AbilityHelper.find_ability(self)

var target_field: Array

func _update_tactics(tactics: Tactics):
	target_field = tactics.fields[target_group_name()]

func _process_property(entity: Entity, map: Map):
	var actor := entity.get_property(Actor) as Actor
	if actor.next_action == null and not target_field.empty():
		var walk := AbilityUse.new(walk_ability, map, entity)
		var pending_cmd = walk.pending_command()
		if pending_cmd != null and pending_cmd is SelectTile:
			var movements := []
			for movement in pending_cmd.get_valid_tiles(walk):
				var field_value := target_field[movement.y][movement.x] as int
				if		field_value <= PERCEPTION_RANGE \
					and Physics.new(map).can_enter_tile(entity, movement):
					movements.append(movement)
			movements.sort_custom(self, "_compare_movements")
			for movement in movements:
				walk = walk.with_target_tile(movement)
				if walk.pending_command() == null:
					actor.next_action = walk
					return

func _compare_movements(tile1: Vector2, tile2: Vector2):
	return self.target_field[tile1.y][tile1.x] \
		 < self.target_field[tile2.y][tile2.x]

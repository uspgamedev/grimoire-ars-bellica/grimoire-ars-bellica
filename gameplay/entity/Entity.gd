class_name Entity extends Sprite

const PLAYER_GROUP = "player"
const MONSTER_GROUP = "monster"
const ENVIRONMENT_GROUP = "environment"
const SENSOR_GROUP = "sensor"

const SMOOTH := 15
const MAX_SPEED := 150

export var entity_name := "Entity"
export var tile := Vector2()
export var movement_profile: Resource

export var DEBUG_EFFECTS := false

onready var queue := []

func _ready():
	self.position = self.tile * Tile.SIZE
	queue.push_back(tile)
	for property in get_children():
		if property.has_method("_entity_ready"):
			property._entity_ready(self)
	_update_groups()

func is_moving(map: TileMap) -> bool:
	var diff: Vector2 = map.map_to_world(self.tile) - self.position
	return is_inside_tree() and diff.length_squared() > 3*3

func _process_entity(map: TileMap, delta: float):
	_update_groups()
	if tile != queue.back():
		queue.push_back(tile)
	var target_tile := queue.front() as Vector2
	var diff: Vector2 = map.map_to_world(target_tile) - self.position
	if diff.length_squared() > 1:
		position += diff.clamped(MAX_SPEED) * delta * SMOOTH
	elif queue.size() > 1:
		queue.pop_front()
	else:
		position = map.map_to_world(tile)
	for property in get_children():
		if property.has_method("_process_property"):
			property._process_property(self, map)

func add_property(property: Node, move_to_first := false):
	add_child(property)
	if move_to_first:
		move_child(property, 0)
	if is_inside_tree():
		property._entity_ready(self)
	_update_groups()

func _update_groups():
	for group in get_groups():
		remove_from_group(group)
	for property in get_children():
		for group in property.get_groups():
			add_to_group(group)

func has_property(property_type: Script) -> bool:
	for property in get_children():
		if property is property_type:
			return true
	return false

func get_property(property_type: Script) -> Node:
	for property in get_children():
		if property is property_type:
			return property
	return null

func get_info() -> String:
	var info := "%s\n\n" % entity_name.to_upper()
	var first := true
	for child in get_children():
		if child.INFO != "":
			if first:
				first = false
				info += child.INFO
			else:
				info += ", " + child.INFO
	return info

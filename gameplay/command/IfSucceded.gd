class_name IfSucceeded extends Command

export var command_path := NodePath()

func perform(use: AbilityUse):
	if has_node(command_path) and (get_node(command_path) as Command).successful:
		for child in get_children():
			if child is Command:
				child.perform(use)
		success()

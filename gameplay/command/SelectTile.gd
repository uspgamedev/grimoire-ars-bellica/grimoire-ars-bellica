tool
class_name SelectTile extends Command

export var RANGE := 1
export var INCLUSIVE := true
export var custom_movement_profile := true
export var movement_profile: Resource

func _ready():
	if movement_profile == null:
		movement_profile = MovementProfile.new()

func get_input_pending(use: AbilityUse) -> int:
	var offset := use.target.target_offset as Vector2
	if offset != Tile.INVALID:
		var target_tile := use.actor_entity.tile + offset
		if is_tile_valid(use.map, use.actor_entity, target_tile):
			return AbilityTarget.FIELD.NONE
	return AbilityTarget.FIELD.TARGET_OFFSET

func evaluate(use: AbilityUse):
	var target_tile := (use.actor_entity.tile + use.target.target_offset) as Vector2
	return preview_target_tile(use.map, use.actor_entity, target_tile)

func get_valid_tiles(use: AbilityUse) -> Array:
	var tiles := []
	for y in range(-RANGE, RANGE + 1):
		for x in range(-RANGE, RANGE + 1):
			var tile = use.actor_entity.tile + Vector2(x, y)
			if is_tile_valid(use.map, use.actor_entity, tile):
				tiles.append(tile)
	return tiles

func is_tile_valid(map: Map, entity: Entity, tile: Vector2) -> bool:
	var distance := Tile.distance(entity.tile, tile)
	if map.is_tile_inside(tile) and distance <= RANGE:
		var real_target_tile = preview_target_tile(map, entity, tile)
		return real_target_tile != null \
		   and (not INCLUSIVE or real_target_tile == tile)
	else:
		return false

func preview_target_tile(map: Map, entity: Entity, tile: Vector2):
	if custom_movement_profile:
		var movement := Physics \
			.new(map) \
			.hitscan(movement_profile, entity.tile, tile, INCLUSIVE) as Array
		if not movement.empty():
			return movement.back()
	else:
		var move_effect := MoveEffect.new().to(tile).on(entity)
		move_effect = EffectSolver.preview(map, move_effect)
		if move_effect.is_succesfull():
			return move_effect.get_tile()
	return null

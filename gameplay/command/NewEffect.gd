tool
class_name NewEffect extends Command

enum TileFieldValue {
	SELF_TILE,
	TARGET_TILE,
	EVAL_TILE
}

enum ValueField {
	CONSTANT,
	EVAL
}

enum DirectionField {
	NONE,
	CONSTANT,
	TARGET_DIRECTION,
}

enum NodePathField {
	NONE,
	SPELL,
	SELF,
	STATUS,
}

var effect_type: String setget _set_effect_type

var tile setget _set_tile_spec
var eval_tile_path := NodePath()
var value: int = ValueField.CONSTANT setget _set_value_spec
var value_constant := 0
var direction: int = DirectionField.NONE setget _set_direction_spec
var direction_constant: int = Tile.DIR.DOWN
var movement_profile: MovementProfile = null
var stat: int = Stats.Type.NONE
var node_path: int = NodePathField.NONE
var scene
var flags := {}

func get_effect_type() -> Script:
	if effect_type != "":
		return load("%s/%s.gd" % [Effect.DIR_PATH, effect_type]) as Script
	else:
		return null

func evaluate(use: AbilityUse):
	var effect := get_effect_type().new() as Effect
	var self_entity := use.actor_entity
	match tile:
		TileFieldValue.SELF_TILE:
			effect.data.tile = self_entity.tile
		TileFieldValue.TARGET_TILE:
			effect.data.tile = self_entity.tile \
							 + use.target.target_offset
		TileFieldValue.EVAL_TILE:
			var cmd := get_node(eval_tile_path) as Command
			var eval_tile := cmd.evaluate(use) as Vector2
			effect.data.tile = eval_tile
	match value:
		ValueField.CONSTANT:
			effect.data.value = value_constant
	match direction:
		DirectionField.CONSTANT:
			effect.data.direction = direction_constant
			effect.data.set_flag(SpawnEffect.DIRECTED)
		DirectionField.TARGET_DIRECTION:
			effect.data.direction = use.target.target_dir
			effect.data.set_flag(SpawnEffect.DIRECTED)
	if movement_profile != null:
		effect.data.movement_profile = movement_profile
	match node_path:
		NodePathField.SPELL:
			effect.data.node_path = use.target.target_spell_path
		NodePathField.SELF:
			effect.data.node_path = use.actor_entity.get_path()
		NodePathField.STATUS:
			effect.data.node_path = use.target.target_status_path
	if EffectData.TYPE.STAT in get_effect_type()._required_fields():
		effect.data.stat = stat
	if scene != null:
		effect.data.scene = scene
	var fx_type := get_effect_type() as Script
	if fx_type == GiveStatusEffect:
		effect.data.node_path = self_entity.get_path()
	if EffectData.TYPE.SUB_EFFECT in fx_type._required_fields():
		var subfx: Effect = null
		for child in get_children():
			if child is Command:
				if subfx == null:
					subfx = child.evaluate(use)
				else:
					subfx = subfx.then(child.evaluate(use)).next
		effect.data.sub_effect = subfx
	for flag in flags:
		if flags[flag]:
			effect.data.flags[flag] = true
	return effect

func _get_property_list():
	var properties := []
	properties.append({
		name = "effect_type",
		type = TYPE_STRING,
		usage = PROPERTY_USAGE_DEFAULT,
		hint = PROPERTY_HINT_ENUM,
		hint_string = _get_all_effect_types()
	})
	var type := get_effect_type()
	if type != null:
		for field_type in type._required_fields():
			if field_type is int:
				match field_type:
					EffectData.TYPE.TILE:
						properties.append({
							name = "tile",
							type = TYPE_INT,
							usage = PROPERTY_USAGE_DEFAULT,
							hint = PROPERTY_HINT_ENUM,
							hint_string = "Self tile,Target tile,Eval tile"
						})
						match tile:
							TileFieldValue.EVAL_TILE:
								properties.append({
									name = "eval_tile_path",
									type = TYPE_NODE_PATH,
									usage = PROPERTY_USAGE_DEFAULT,
									hint = PROPERTY_HINT_NONE
								})
					EffectData.TYPE.VALUE:
						properties.append({
							name = "value",
							type = TYPE_INT,
							usage = PROPERTY_USAGE_DEFAULT,
							hint = PROPERTY_HINT_ENUM,
							hint_string = "Constant,Eval"
						})
						match value:
							ValueField.CONSTANT:
								properties.append({
									name = "value_constant",
									type = TYPE_INT,
									usage = PROPERTY_USAGE_DEFAULT,
									hint = PROPERTY_HINT_NONE
								})
					EffectData.TYPE.DIRECTION:
						properties.append({
							name = "direction",
							type = TYPE_INT,
							usage = PROPERTY_USAGE_DEFAULT,
							hint = PROPERTY_HINT_ENUM,
							hint_string = "None,Constant,Target Dir"
						})
						match direction:
							DirectionField.CONSTANT:
								properties.append({
									name = "direction_constant",
									type = TYPE_INT,
									usage = PROPERTY_USAGE_DEFAULT,
									hint = PROPERTY_HINT_ENUM,
									hint_string = Tile.DIR_HINT
								})
					EffectData.TYPE.MOVEMENT_PROFILE:
						properties.append({
							name = "movement_profile",
							type = TYPE_OBJECT,
							usage = PROPERTY_USAGE_DEFAULT,
							hint = PROPERTY_HINT_RESOURCE_TYPE,
							hint_string = "MovementProfile"
						})
					EffectData.TYPE.NODE_PATH:
						properties.append({
							name = "node_path",
							type = TYPE_INT,
							usage = PROPERTY_USAGE_DEFAULT,
							hint = PROPERTY_HINT_ENUM,
							hint_string = "None,Spell,Self,Status"
						})
					EffectData.TYPE.STAT:
						properties.append({
							name = "stat",
							type = TYPE_INT,
							usage = PROPERTY_USAGE_DEFAULT,
							hint = PROPERTY_HINT_ENUM,
							hint_string = Stats.enum_hint()
						})
					EffectData.TYPE.SCENE:
						properties.append({
							name = "scene",
							type = TYPE_OBJECT,
							usage = PROPERTY_USAGE_DEFAULT,
							hint = PROPERTY_HINT_RESOURCE_TYPE,
							hint_string = "PackedScene"
						})
			elif field_type is String:
				properties.append({
					name = "trait/" + field_type,
					type = TYPE_BOOL,
					usage = PROPERTY_USAGE_DEFAULT,
					hint = PROPERTY_HINT_NONE,
				})
	return properties

func _get_all_effect_types() -> String:
	var types := []
	var dir := Directory.new()
	if dir.open(Effect.DIR_PATH) == OK:
		#warning-ignore:return_value_discarded
		dir.list_dir_begin()
		var file_name := dir.get_next()
		while file_name != "":
			if not dir.current_is_dir():
				var script := load(Effect.DIR_PATH + "/" + file_name) as Script
				if script.get_base_script() == Effect:
					types.append(file_name.trim_suffix(".gd"))
			file_name = dir.get_next()
		dir.list_dir_end()
	types.sort()
	var hint := ""
	for file in types:
		if hint != "":
			hint += ","
		hint += file
	return hint

func _set_effect_type(type: String):
	effect_type = type
	if EffectData.TYPE.MOVEMENT_PROFILE in get_effect_type()._required_fields():
		movement_profile = MovementProfile.new()
	property_list_changed_notify()

func _set_tile_spec(spec: int):
	tile = spec
	property_list_changed_notify()

func _set_value_spec(spec: int):
	value = spec
	property_list_changed_notify()

func _set_direction_spec(spec: int):
	direction = spec
	property_list_changed_notify()

func _get(property: String):
	if property.begins_with("trait/"):
		var key = property.trim_prefix("trait/")
		return flags.get(key, false)

func _set(property: String, flag):
	if property.begins_with("trait/"):
		var key = property.trim_prefix("trait/")
		flags[key] = flag

class_name OnEnvironment extends Command

func perform(use: AbilityUse):
	if get_child_count() < 1: return
	var env := use.map.get_environment()
	for cmd in get_children():
		if cmd is NewEffect:
			var effect := cmd.evaluate(use) as Effect
			effect = effect.on(env)
			if not EffectSolver.resolve(use.map, effect).is_succesfull():
				return
	success()

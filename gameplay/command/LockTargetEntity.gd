class_name LockTargetEntity extends Command

func perform(use: AbilityUse):
	var entity := use.actor_entity
	var target_tile := entity.tile + use.target.target_offset
	var target_entity := use.map.find_entity_at(target_tile)
	if target_entity != null:
		use.target.target_entity_path = target_entity.get_path()
		success()

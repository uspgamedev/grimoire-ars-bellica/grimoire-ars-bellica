class_name Command extends Node

const KEY_CHECK := AbilityUse.CMD_KEY

onready var successful := false

func success():
	successful = true

func prepare():
	successful = false

func get_input_pending(_use: AbilityUse) -> int:
	return AbilityTarget.FIELD.NONE

func evaluate(_use: AbilityUse):
	return null

func perform(_use: AbilityUse):
	success()

class_name SelectDirection extends Command

enum Referential {
	SELF_TILE,
	TARGET_TILE
}

export(Referential) var referential = Referential.SELF_TILE

var selected_dir = null

func get_input_pending(use: AbilityUse) -> int:
	if use.target.target_dir == Tile.DIR.ALL:
		return AbilityTarget.FIELD.TARGET_DIR
	else:
		return AbilityTarget.FIELD.NONE

func get_referential_tile(use: AbilityUse) -> Vector2:
	match referential:
		Referential.SELF_TILE:
			return use.actor_entity.tile
		Referential.TARGET_TILE:
			return use.actor_entity.tile + use.target.target_offset
	return Tile.INVALID

func evaluate(use: AbilityUse):
	return use.target.target_dir

class_name Rule extends Node

const DIRTY_RULE_GROUP = "dirty_rule_group"

export(Array, Script) var EFFECT_TYPES
export var PROCESSES_EFFECT := false
export var APPLIES_EFFECT := false

var entity_path: NodePath
var property_path: NodePath
var processed_effects := {}
var applied_effects := {}

func accepts(effect: Effect) -> bool:
	for type in EFFECT_TYPES:
		if effect is type:
			return true
	return false

func _property_ready(entity: Entity, property: Property):
	entity_path = entity.get_path()
	property_path = property.get_path()

func get_self_entity() -> Entity:
	if has_node(entity_path):
		return get_node(entity_path) as Entity
	else:
		return null

func get_self_property() -> Property:
	if has_node(property_path):
		return get_node(property_path) as Property
	else:
		return null

func process_effect(map: Map, effect: Effect) -> Effect:
	if accepts(effect) and not processed_effects.has(effect):
		processed_effects[effect] = true
		add_to_group(DIRTY_RULE_GROUP)
		return _process_effect(map, effect)
	else:
		return effect

func _process_effect(_map: Map, effect: Effect) -> Effect:
	return effect

func apply_effect(map: Map, effect: Effect):
	if accepts(effect) and not applied_effects.has(effect):
		applied_effects[effect] = true
		add_to_group(DIRTY_RULE_GROUP)
		_apply_effect(map, effect)

func _apply_effect(_map: Map, _effect: Effect):
	pass

static func clear_all_rules(tree: SceneTree):
	for rule in tree.get_nodes_in_group(DIRTY_RULE_GROUP):
		rule.processed_effects.clear()
		rule.applied_effects.clear()
		(rule as Node).remove_from_group(DIRTY_RULE_GROUP)

func _preview_effect(_map: Map, _effect: Effect):
	pass

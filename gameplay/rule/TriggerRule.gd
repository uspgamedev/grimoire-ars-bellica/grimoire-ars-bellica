class_name TriggerRule extends Rule

const GROUP_NAME := "trigger-rule"

signal ability_triggered(ability)

onready var triggered_ability := AbilityHelper.find_ability(self)

func _ready():
	add_to_group(GROUP_NAME)

func trigger_ability(map: Map, data := {}):
	var ability := AbilityUse.new(triggered_ability, map, \
								  get_self_entity()).with(data)
	emit_signal("ability_triggered", ability)

class_name Spell extends Node

export(String) var spell_name = "<empty>"
export(Texture) var spell_icon
export(String, MULTILINE) var description = ""

onready var main_ability := AbilityHelper.find_ability(self)

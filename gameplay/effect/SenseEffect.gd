class_name SenseEffect extends Effect

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.EFFECT_LINK,
		EffectData.TYPE.NODE_PATH
	]

func get_class(): return "SenseEffect"

func senses(effect: Effect) -> SenseEffect:
	data.effect_link = weakref(effect)
	return self

func about(entity: Entity) -> SenseEffect:
	data.node_path = entity.get_path()
	return self

func get_sensed_effect() -> Effect:
	return data.effect_link.get_ref() as Effect

func get_subject_path() -> NodePath:
	return data.node_path

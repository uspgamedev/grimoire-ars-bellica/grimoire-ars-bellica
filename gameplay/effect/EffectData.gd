class_name EffectData extends Reference

const PREVIEW_FLAG := "preview"

var tile := Tile.INVALID
var value := 0
var direction: int = Tile.DIR.ALL
var movement_profile: MovementProfile = null
var stat: int = Stats.Type.NONE
var scene: PackedScene
var node_path := NodePath()
var effect_link: WeakRef
var sub_effect: Reference
var flags := {}

enum TYPE {
	TILE,
	VALUE,
	DIRECTION,
	MOVEMENT_PROFILE,
	STAT,
	SCENE,
	NODE_PATH,
	EFFECT_LINK,
	SUB_EFFECT
}

func _init():
	flags = {}

func set_flag(flag: String):
	flags[flag] = true

func has_flag(flag: String) -> bool:
	return flags.get(flag, false)

func copy_fields_to(obj):
	if tile != Tile.INVALID: obj.tile = tile
	if value != 0: obj.value = value
	if has_flag("directed"): obj.direction = direction
	if movement_profile != null: obj.movement_profile = movement_profile
	if stat != Stats.Type.NONE: obj.stat = stat
	if scene != null: obj.scene = scene
	if not node_path.is_empty(): obj.node_path = node_path
	if effect_link != null: obj.effect_link = effect_link
	if sub_effect != null: obj.sub_effect = sub_effect
	if not flags.empty(): obj.flags = flags.duplicate()

func duplicate() -> EffectData:
	var other := get_script().new() as EffectData
	copy_fields_to(other)
	return other

class_name SpawnEffect extends Effect

const AUTO_SPILL = "auto_spill"
const DIRECTED = "directed"

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.TILE,
		EffectData.TYPE.DIRECTION,
		EffectData.TYPE.SCENE,
		EffectData.TYPE.NODE_PATH,
		"controlled",
		AUTO_SPILL
	]

func get_class(): return "SpawnEffect"

func spawn_entity(scene: PackedScene) -> SpawnEffect:
	data.scene = scene
	return self

func from(entity: Entity) -> SpawnEffect:
	data.node_path = entity.get_path()
	return self

func at(tile: Vector2) -> SpawnEffect:
	data.tile = tile
	return self

func facing(dir: int) -> SpawnEffect:
	data.direction = dir
	data.flags.directed = true
	return self

func maintain_control() -> SpawnEffect:
	data.flags.controlled = true
	return self

func with_auto_spill() -> SpawnEffect:
	data.flags[AUTO_SPILL] = true
	return self

func get_spawned_entity() -> Entity:
	return data.scene.instance() as Entity

func get_spawner_path() -> NodePath:
	return data.node_path

func get_spawn_tile() -> Vector2:
	return data.tile

func has_dir() -> bool:
	return data.flags.get(DIRECTED, false)

func get_spawn_dir() -> int:
	return data.direction

func is_controlled() -> bool:
	return data.flags.get("controlled", false)

func is_auto_spill() -> bool:
	return data.flags.get(AUTO_SPILL, false)

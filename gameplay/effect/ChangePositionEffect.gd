class_name ChangePositionEffect extends Effect

static func _required_fields() -> Array:
	return [
		EffectData.TYPE.NODE_PATH,
		EffectData.TYPE.TILE
	]

func get_class(): return "ChangePositionEffect"

func relocate(entity: Entity) -> ChangePositionEffect:
	data.node_path = entity.get_path()
	return self

func to(tile: Vector2) -> ChangePositionEffect:
	data.tile = tile
	return self

func get_moved_entity_path() -> NodePath:
	return data.node_path

func get_tile() -> Vector2:
	return data.tile

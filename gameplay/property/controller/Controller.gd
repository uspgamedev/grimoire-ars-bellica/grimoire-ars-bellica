class_name Controller extends Property

export var controlled_paths := []

func _save(storage: Dictionary):
	storage["controlled_paths"] = controlled_paths.duplicate()

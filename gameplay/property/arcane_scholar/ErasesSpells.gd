extends Rule

func _apply_effect(_map: Map, effect: Effect):
	var erase_effect := effect as EraseSpellEffect
	var scholar := get_self_property() as ArcaneScholar
	var spell := get_node_or_null(erase_effect.get_spell_path()) as Spell
	scholar.erase_spell(spell)

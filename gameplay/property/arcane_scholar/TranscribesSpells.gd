extends Rule

func _apply_effect(_map: Map, effect: Effect):
	var transcribe_effect := effect as TranscribeSpellEffect
	var spell_scn := transcribe_effect.get_spell()
	var scholar := get_self_entity() \
		.get_property(ArcaneScholar) as ArcaneScholar
	if scholar != null and scholar.can_store_spell(spell_scn):
		scholar.store_spell(spell_scn)

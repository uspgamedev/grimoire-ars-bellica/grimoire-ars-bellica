class_name AreaSensor extends Property

const OTHER_ENTERED = "other_entered"
const OTHER_EXITED = "other_exited"
const SELF_ENTERED = "self_entered"
const SELF_EXITED = "self_exited"

export(int, 0, 10) var radius = 0

func _ready():
	add_to_group(Entity.SENSOR_GROUP)

func _save(storage: Dictionary):
	storage["radius"] = radius

static func entered_area(effect: Effect) -> bool:
	return effect.data.has_flag(OTHER_ENTERED) \
		or effect.data.has_flag(SELF_ENTERED)

static func exited_area(effect: Effect) -> bool:
	return effect.data.has_flag(OTHER_EXITED) \
		or effect.data.has_flag(SELF_EXITED)

class_name AI extends Property

enum TargetGroup {
	PLAYER, MONSTER
}

const GROUP_NAME := {
	TargetGroup.PLAYER: Entity.PLAYER_GROUP,
	TargetGroup.MONSTER: Entity.MONSTER_GROUP
}

export(TargetGroup) var target_group

func target_group_name() -> String:
	return GROUP_NAME[target_group]

func find_nearest(entity: Entity) -> Entity:
	var nearest: Entity = null
	var mindist := Tile.MAX_DIST
	for player_entity in get_tree().get_nodes_in_group(target_group_name()):
		if player_entity is Entity and player_entity.has_property(Destructible) \
								   and player_entity.has_property(Actor):
			var player_tile := player_entity.tile as Vector2
			var self_tile := entity.tile
			var dist := Tile.distance(player_tile, self_tile)
			if dist < mindist:
				nearest = player_entity
				mindist = dist
	return nearest


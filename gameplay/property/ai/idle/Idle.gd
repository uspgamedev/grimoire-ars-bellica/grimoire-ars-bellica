extends AI

onready var wait_ability := AbilityHelper.find_ability(self)

func _process_property(entity: Entity, map: Map):
	var actor := entity.get_property(Actor) as Actor
	if actor.next_action == null:
			var wait := AbilityUse.new(wait_ability, map, entity)
			if wait.pending_command() == null:
				actor.next_action = wait

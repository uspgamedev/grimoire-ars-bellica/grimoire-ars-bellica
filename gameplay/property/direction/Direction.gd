extends Property

class_name Direction

export(Tile.DIR) var direction: int

export(Array, Texture) var FACES

func _process_property(entity: Entity, _map: Map):
	if FACES.size() >= Tile.DIR.ALL:
		entity.texture = FACES[self.direction]

func _save(storage: Dictionary):
	storage["direction"] = direction

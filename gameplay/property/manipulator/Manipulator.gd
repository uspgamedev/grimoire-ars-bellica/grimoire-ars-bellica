class_name Manipulator extends Property

export var reach := 1

func _available_abilities(map: Map) -> Array:
	var owner_entity := get_node(owner_path)
	var abilities := []
	for entity in map.get_all_entities():
		var dist := Tile.distance(entity.tile, owner_entity.tile)
		if dist <= reach and entity.has_property(Interactive):
			var interactive := entity.get_property(Interactive) as Interactive
			abilities.append({
				name = interactive.interaction_prompt,
				ability = interactive.ability,
				data = {}
			})
	return abilities

func _save(storage: Dictionary):
	storage["reach"] = reach

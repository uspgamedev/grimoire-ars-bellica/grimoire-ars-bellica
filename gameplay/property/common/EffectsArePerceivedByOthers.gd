extends Rule

func _apply_effect(map: Map, effect: Effect):
	if effect is SenseEffect: return
	var sense_effect := effect
	for entity in map.get_all_entities():
		if entity.get_path() != entity_path:
			sense_effect = sense_effect.then(
				SenseEffect.new()\
					.senses(effect)\
					.on(entity)
			)
			sense_effect = sense_effect.next

extends Rule

func _process_effect(map: Map, effect: Effect) -> Effect:
	var line_effect := effect as LineEffect
	var target_effect := line_effect.get_caused_effect() as TargetEffect
	if target_effect == null:
		return effect.prevent()
	var origin_tile := line_effect.get_tile()
	var dir := line_effect.get_direction()
	var length := line_effect.get_length()
	var end_tile := origin_tile + (Tile.FROM_DIR[dir] * length) as Vector2
	var profile := line_effect.get_movement_profile()
	var each_effect := line_effect as Effect
	var physics := Physics.new(map)
	var tiles := physics.hitscan(profile, origin_tile, end_tile)
	for tile in tiles:
		for entity in map.find_all_entities_at(tile):
			each_effect = each_effect.then(
				target_effect \
					.duplicate() \
					.target(entity) \
					.on(line_effect.affected_entity)
			)
			each_effect = each_effect.next
	return effect

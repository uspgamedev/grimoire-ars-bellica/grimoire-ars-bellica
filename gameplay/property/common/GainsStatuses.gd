extends Rule

signal message(text)

func _apply_effect(_map: Map, effect: Effect):
	var give_status_effect := effect as GiveStatusEffect
	var status_scn := give_status_effect.get_status()
	var status := status_scn.instance() as Status
	var duration := give_status_effect.get_duration()
	if duration > 0:
		status.DURATION = duration + 1
	status.SOURCE_ENTITY_PATH = give_status_effect.get_source_path()
	var entity := get_self_entity()
	entity.add_property(status, true)
	if status.INFO != "":
		emit_signal("message", "%s is now %s" % [entity.entity_name, \
												 status.INFO])

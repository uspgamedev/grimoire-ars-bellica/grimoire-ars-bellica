class_name Actor extends Property

export var SPEED := 0

var next_action: AbilityUse = null

onready var wait := AbilityHelper.find_ability(self)

func pop_next_action() -> AbilityUse:
	var action = next_action
	next_action = null
	return action

func _save(storage: Dictionary):
	storage["SPEED"] = SPEED

class_name Stats extends Object

enum Type {
	NONE, HIT_POINTS
}

static func enum_hint() -> String:
	return "None,Hit Points"

static func damage_modifier_for(battle: Battle, command: Command) -> int:
	var tree := battle.get_tree()
	var mod := 0
	for modifier in tree.get_nodes_in_group("damage-modifier"):
		mod += modifier._applied_to(battle, command)
	return mod;

static func heal_modifier_for(battle: Battle, command: Command) -> int:
	var tree := battle.get_tree()
	var mod := 0
	for modifier in tree.get_nodes_in_group("heal-modifier"):
		mod += modifier._applied_to(battle, command)
	return mod;

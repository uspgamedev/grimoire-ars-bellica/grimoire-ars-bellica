class_name Tactics extends Node

const MAX_VALUE := 99

export var map_path := NodePath("")

onready var fields := {
	Entity.PLAYER_GROUP: [],
	Entity.MONSTER_GROUP: [],
}

onready var last_player_pos := Vector2(-1, -1)

func _ready():
	for field in fields.values():
		clear_field(field, 0)

func clear_field(field: Array, value: int):
	var map := get_node(self.map_path) as Map
	field.resize(map.SIZE)
	for i in range(map.SIZE):
		if field[i] == null:
			field[i] = []
			field[i].resize(map.SIZE)
		for j in range(map.SIZE):
			field[i][j] = value

func _update():
	var map := get_node(self.map_path) as Map
	# Update player field
	var player := map.get_player()
	if player != null and player.tile != self.last_player_pos:
		self.last_player_pos = player.tile
		var tiles := [player.tile]
		var field := fields[Entity.PLAYER_GROUP] as Array
		generate_field(map, field, tiles)
	# Update monster field
	var tiles := []
	for monster in get_tree().get_nodes_in_group(Entity.MONSTER_GROUP):
		if monster is Entity:
			tiles.append(monster.tile)
	generate_field(map, fields[Entity.MONSTER_GROUP], tiles)
	for entity in map.get_all_entities():
		entity.propagate_call("_update_tactics", [self])

func generate_field(map: Map, field: Array, tiles: Array):
	var queue := []
	clear_field(field, MAX_VALUE)
	for tile in tiles:
		queue.push_back(tile)
		field[tile.y][tile.x] = 0
	while not queue.empty():
		var tile := queue.pop_front() as Vector2
		var value = field[tile.y][tile.x]
		for dir in Tile.FROM_DIR:
			var next := tile + (dir as Vector2)
			if not map.is_blocked_tile(next):
				var next_value = field[next.y][next.x]
				if next_value > value + 1:
					queue.push_back(next)
					field[next.y][next.x] = value + 1
	#print_field(field)

static func print_field(field: Array):
	for row in field:
		var line := ""
		for value in row:
			line += " %2d" % value
		print(line)

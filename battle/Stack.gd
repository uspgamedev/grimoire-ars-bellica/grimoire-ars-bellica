class_name Stack extends Node

export var first_state_path: NodePath

signal state_changed(state)

onready var stack := []
onready var states := {}

func _ready():
	for state in get_children():
		states[state.get_script()] = state

func empty() -> bool:
	return stack.empty()

func clear():
	stack.clear()

func current() -> Node:
	return get_node_or_null(stack.back())

func bootstrap():
	push(get_node(first_state_path).get_path())

func push(name: String):
	if has_node(name):
		var next := get_node(name)
		var previous: Node = null
		if !self.stack.empty():
			previous = current()
			previous._suspend()
		stack.push_back(next.get_path())
		current()._enter(previous)
		emit_signal("state_changed", current())

func pop():
	current()._leave()
	var previous = current()
	stack.pop_back()
	if !self.stack.empty():
		current()._resume(previous)
		emit_signal("state_changed", current())

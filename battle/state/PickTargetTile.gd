class_name PickTargetTile extends PickTarget

func _enter(previous: State):
	._enter(previous)
	var select_tile := command as SelectTile
	var tiles := select_tile.get_valid_tiles(ability_use)
	battle.get_map().highlight(tiles)
	battle.get_ui().set_special_action_hint("---")

func _on_map_tile_clicked(request: MapTileClickedRequest):
	if request.tile in command.get_valid_tiles(ability_use):
		ability_use = ability_use.with_target_tile(request.tile)
		success = true
		pop()

func _on_process(_request: ControlRequest):
	if not battle.get_map().has_entities_moving():
		battle.get_ui().set_primary_action_hint("TARGET")
		battle.get_ui().set_secondary_action_hint("CANCEL")

func get_hint() -> String:
	return "Cast here"

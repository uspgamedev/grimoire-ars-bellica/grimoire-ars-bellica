class_name PlayTurn extends State

enum TurnState {
	BEGIN, ACT, END
}

export var BEGINTURN_ABILITY: PackedScene
export var ENDTURN_ABILITY: PackedScene

onready var entity_path: NodePath = ""
onready var state: int = TurnState.BEGIN
onready var beginturn_ability := BEGINTURN_ABILITY.instance() as Ability
onready var endturn_ability := ENDTURN_ABILITY.instance() as Ability

func _ready():
	call_deferred("add_child", beginturn_ability)
	call_deferred("add_child", endturn_ability)

func _enter(_previous: State):
	state = TurnState.BEGIN
	var beginturn = AbilityUse.new(beginturn_ability, battle.get_map(), \
								   get_node(entity_path))
	get_state(ResolveAbility).queue_ability(beginturn).push()

func _resume(previous: State):
	if previous is ResolveAbility:
		if not has_node(self.entity_path):
			pop()
			return
		var entity := get_node(self.entity_path) as Entity
		var actor := entity.get_property(Actor) as Actor
		match state:
			TurnState.BEGIN:
				if actor != null:
					state = TurnState.ACT
				else:
					end_turn()
			TurnState.ACT:
				end_turn()
			TurnState.END:
				pop()

func end_turn():
	state = TurnState.END
	var endturn = AbilityUse.new(endturn_ability, battle.get_map(), \
								 get_node(entity_path))
	get_state(ResolveAbility).queue_ability(endturn).push()

func _on_process(_request: ControlRequest):
	if state != TurnState.ACT:
		return
	var entity := get_node_or_null(entity_path) as Entity
	if entity == null:
		return
	var actor := entity.get_property(Actor) as Actor
	var action = actor.pop_next_action()
	if action != null:
		get_state(ResolveAbility).queue_ability(action).push()

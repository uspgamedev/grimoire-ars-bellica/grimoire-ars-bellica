class_name State extends Node

var battle: Battle = null

func _battle_ready(the_battle: Battle):
	battle = the_battle

func get_state(state_class: Script) -> State:
	return battle.stack.states.get(state_class) as State

func push():
	battle.stack.push(name)

func pop():
	battle.stack.pop()

func _enter(_previous: State):
	pass

func _suspend():
	pass

func _resume(_previous: State):
	pass

func _leave():
	pass

func get_hint() -> String:
	return HintContainer.UNDEFINED_HINT

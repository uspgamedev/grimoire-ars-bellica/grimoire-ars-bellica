class_name BattleRound extends State

onready var entity_queue: Array = []
onready var current_entity_path := NodePath()

func _enter(_previous: State):
	entity_queue.clear()
	new_round()

func _resume(_previous: State):
	if not all_actors_ready():
		return
	var removed := []
	for i in entity_queue.size():
		var path := entity_queue[i] as NodePath
		var entity := get_node_or_null(path) as Entity
		if entity != null:
			var initiative_effect := RollInitiativeEffect.new().on(entity)
			if EffectSolver.resolve(battle.get_map(), initiative_effect).is_succesfull():
				pass
		else:
			removed.append(i)
	var n := removed.size()
	for k in n:
		entity_queue.remove(removed[n - k - 1])
	entity_queue.sort_custom(self, "sort_by_initiative")

func _on_process(_request: ControlRequest):
	if all_actors_ready() and !next_turn():
		new_round()

func new_round():
	battle.mark_dirty()
	battle.turn += 1
	var entities := battle.get_map().get_all_entities()
	if entities.empty():
		print("No more entities")
		return get_tree().quit()
	battle.get_tactics()._update()
	entity_queue.clear()
	for entity in entities:
		var round_effect := NewRoundEffect.new().on(entity)
		if not EffectSolver.resolve(battle.get_map(), round_effect).is_succesfull():
			pass
		if (entity as Entity).has_property(Turn):
			entity_queue.append(entity.get_path())
			if (entity as Entity).has_property(Actor):
				var actor := entity.get_property(Actor) as Actor
				#warning-ignore:return_value_discarded
				actor.pop_next_action()
	get_state(ResolveAbility).push()

func sort_by_initiative(path1, path2) -> bool:
	var turn1 := get_node(path1).get_property(Turn) as Turn
	var turn2 := get_node(path2).get_property(Turn) as Turn
	return turn1.initiative > turn2.initiative

func all_actors_ready() -> bool:
	for entity_path in entity_queue:
		if !has_node(entity_path):
			continue
		var entity := get_node(entity_path) as Entity
		if entity.has_property(Turn) and entity.has_property(Actor):
			if entity.get_property(Actor).next_action == null:
				if entity.has_property(Player):
					get_state(ChooseAction).push()
				return false
	return true

func next_turn():
	while !entity_queue.empty():
		var entity_path = entity_queue.front()
		if !has_node(entity_path):
			entity_queue.pop_front()
			continue
		var entity = get_node(entity_path)
		if entity.is_queued_for_deletion() or !entity.has_property(Turn):
			entity_queue.pop_front()
			continue
		entity_queue.pop_front()
		current_entity_path = entity_path
		var play_turn_state := get_state(PlayTurn) as PlayTurn
		play_turn_state.entity_path = entity_path
		play_turn_state.push()
		return true
	return false

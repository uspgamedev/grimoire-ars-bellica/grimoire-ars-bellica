class_name Confirm extends State

var query := "Are you sure?"
var confirmed := false

func _enter(_previous: State):
	battle.get_ui().get_confirm_popup().display(query)

func _leave():
	battle.get_ui().get_confirm_popup().hide()

func _on_confirm(_request: ControlRequest):
	confirmed = true
	pop()

func _on_cancel(_request: ControlRequest):
	confirmed = false
	pop()

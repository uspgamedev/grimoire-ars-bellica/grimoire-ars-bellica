class_name PickTargetDirection extends PickTarget

var current_dir: int = Tile.DIR.ALL

func _enter(previous: State):
	._enter(previous)
	battle.get_ui().set_special_action_hint("---")

func _on_map_tile_clicked(_request: ControlRequest):
	ability_use = ability_use.with_target_direction(current_dir)
	success = true
	pop()

func _on_process(_request: ControlRequest):
	var map := battle.get_map()
	var cursor_tile = map.get_cursor_tile()
	var ref_tile := (command as SelectDirection).get_referential_tile(ability_use)
	var diff = cursor_tile - ref_tile
	var dir = Tile.to_closest_dir(diff)
	if dir != current_dir:
		var path := []
		var step := Tile.FROM_DIR[dir] as Vector2
		for i in Map.SIZE:
			var next := (ref_tile + (i + 1) * step) as Vector2
			if map.info.is_tile_inside_map(next):
				path.append(next)
			else:
				break
		current_dir = dir
		map.highlight(path)
	if not map.has_entities_moving():
		battle.get_ui().set_primary_action_hint("DIRECTION")
		battle.get_ui().set_secondary_action_hint("CANCEL")

func get_hint() -> String:
	return "Towards here"

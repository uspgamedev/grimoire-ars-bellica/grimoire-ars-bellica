class_name PrepareSpell extends State

var current_spell: Spell
var use: AbilityUse

func set_spell(spell: Spell) -> PrepareSpell:
	current_spell = spell
	return self

func _enter(_previous: State):
	load_spell_ability()

func load_spell_ability():
	var ability = current_spell.main_ability
	var player := battle.get_map().get_player()
	use = AbilityUse.new(ability, battle.get_map(), player)
	battle.get_ui().show_description(current_spell.description)
	process_ability()

func _leave():
	current_spell = null
	battle.get_ui().show_description("")
	clear_ui()

func clear_ui():
	for child in battle.get_ui().spell_list.get_children():
		if child is SpellButton:
			if current_spell != null:
				child.pressed = child.spell_path == current_spell.get_path()
			else:
				child.pressed = false

func _resume(previous: State):
	var pick_target := previous as PickTarget
	if pick_target.success:
		process_ability()
	elif pick_target.alternate_spell != null:
		use = null
		current_spell = pick_target.alternate_spell
		clear_ui()
		load_spell_ability()
	else:
		use = null
		pop()

func process_ability():
	var pending_cmd = use.pending_command()
	if pending_cmd != null:
		match pending_cmd.get_script():
			SelectTile:
				get_state(PickTargetTile) \
					.with_command(pending_cmd, use) \
					.push()
			SelectDirection:
				get_state(PickTargetDirection) \
					.with_command(pending_cmd, use) \
					.push()
	else:
		pop()

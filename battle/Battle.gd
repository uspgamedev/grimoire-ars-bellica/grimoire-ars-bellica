class_name Battle extends Control

signal battle_won(battle)
signal battle_lost(battle)
signal map_exited(dir_vec)
signal save_requested
signal game_quit

onready var stack := $Stack as Stack
onready var turn := 0

var dirty_state := true

var grimoire_path := NodePath("")

func _ready():
	$UI.show_description("")
	$Stack.propagate_call("_battle_ready", [self])
	var player := get_map().get_player()
	if player != null:
		_on_player_added_to_map(player)
	var map := get_map()
	var map_pos := map.position
	var diff := Vector2.UP * 1000
	map.position = map_pos + diff
	$Tween.interpolate_property(map, "position", map.position, map_pos, \
								1.0, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.start()

func is_dirty() -> bool:
	return dirty_state

func mark_dirty():
	dirty_state = true

func mark_clean():
	dirty_state = false

func _on_player_added_to_map(player: Entity):
	var scholar := player.get_property(ArcaneScholar) as ArcaneScholar
	if scholar != null:
		load_spells_to_ui(player)
		SignalHelper.safe_connect(scholar, "grimoire_changed", self, \
									"load_spells_to_ui", [player])

func load_spells_to_ui(player: Entity):
	var scholar := player.get_property(ArcaneScholar) as ArcaneScholar
	if scholar != null:
		$UI.load_spells(scholar.get_spell_list())

func _exit_tree():
	stack.clear()

func get_map() -> Map:
	return $Map as Map

func get_tactics() -> Tactics:
	return $Tactics as Tactics

func get_ui() -> UI:
	return $UI as UI

func get_current_turn_entity_path() -> Entity:
	return $Stack/BattleRound.current_entity_path

func get_current_turn_order() -> Array:
	return $Stack/BattleRound.entity_queue

func _process(_delta):
	if stack.empty():
		stack.bootstrap()
	$Label.text= self.stack.current().name
	if player_lost():
		emit_signal("battle_lost", self)
		return
	if !get_map().is_animating():
		_on_control_request("process", ControlRequest.new())
	var player := get_map().get_player()
	if player != null:
		$UI.propagate_call("_update_info", [self], true)

func is_map_safe() -> bool:
	return get_tree().get_nodes_in_group("monster").empty()

func player_lost() -> bool:
	return get_map().get_player() == null

func exit_map(dir_vec: Vector2):
	emit_signal("map_exited", dir_vec)

func win():
	emit_signal("battle_won", self)

func request_save():
	emit_signal("save_requested")

func _on_control_request(name: String, request: ControlRequest):
	var state = self.stack.current()
	var method_name = "_on_" + name
	if state.has_method(method_name):
		state.call(method_name, request)

func _on_exit_requested():
	emit_signal("game_quit")

tool
class_name EntityAmountGenerator extends EntityGenerator

export(Array, String, "grass", "dirt") var on_tile_type = []
export var entity_scn: PackedScene
export(int, 1, 50) var min_amount := 1
export(int, 1, 50) var max_amount := 1

func _init():
	resource_name = "EntityAmountGenerator"

func generate(map: TileMap) -> Array:
	var generated := []
	var tiles := []
	for tile_name in on_tile_type:
		var tile_id := map.tile_set.find_tile_by_name(tile_name)
		tiles.append_array(map.get_used_cells_by_id(tile_id))
	var variance := max_amount - min_amount
	var amount := min_amount
	if variance > 0:
		amount += randi() % variance
	for i in amount:
		if tiles.empty(): break
		var idx := (randi() % tiles.size()) as int
		var tile := tiles[idx] as Vector2
		tiles.remove(idx)
		generated.append({
			tile = tile,
			entity_scn = entity_scn
		})
	return generated

tool
class_name EntityDensityGenerator extends EntityGenerator

export(String, "any", "grass", "dirt") var on_tile_type = "any"
export var entity_scn: PackedScene
export(float, 0.01, 1.0) var density: float = 0.5

func _init():
	resource_name = "EntityDensityGenerator"

func generate(map: TileMap) -> Array:
	var generated := []
	var tiles
	if on_tile_type == "any":
		tiles = map.get_used_cells()
	else:
		var tile_id := map.tile_set.find_tile_by_name(on_tile_type)
		tiles = map.get_used_cells_by_id(tile_id)
	for tile in tiles:
		if randf() < density:
			generated.append({
				tile = tile,
				entity_scn = entity_scn
			})
	return generated

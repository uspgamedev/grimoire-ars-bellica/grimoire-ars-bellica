tool
class_name Map extends TileMap

const VFX_GROUP = "vfx"
const SIZE := 21

const BLOCKING_TILES := [
	"water_middle",
	"water_border",
	"water_outer_corner",
	"water_inner_corner",
	"forest",
	"wall",
	"empty",
	"cliff_edge",
	"cliff_outer_corner",
	"cliff_inner_corner"
]

export var player_path: NodePath
export var environment_path: NodePath
export var biome: Resource
export var environment_scn: PackedScene
export var CURSOR_SMOOTH := 15

signal player_added(player)
signal cursor_position_set(tile_position, position_info, is_inside_map)
signal control_requested(name, params)
signal exited_map(dir_vec)
signal win

onready var info: MapInfo = MapInfo.new(SIZE, CURSOR_SMOOTH)
onready var cursor: Node2D = $Cursor
onready var cursor_controller: CursorController = CursorController.new(
		self, self.info, self.cursor)

func screen_to_tile(screen_position: Vector2) -> Vector2:
	return world_to_map(
			(screen_position - self.global_position) / self.global_scale)

func load_environment():
	var env := environment_scn.instance() as Entity
	add_entity_at(env, Vector2.ZERO)

func _ready():
	assert(cursor_controller.connect(
			"cursor_tile_set", self, "_on_cursor_tile_set") == OK)
	for player in get_tree().get_nodes_in_group(Entity.PLAYER_GROUP):
		if player is Entity:
			player_path = player.get_path()
	for env in get_tree().get_nodes_in_group(Entity.ENVIRONMENT_GROUP):
		if env is Entity:
			environment_path = env.get_path()
	if Engine.editor_hint and biome == null:
		biome = Biome.new()

func _process(delta: float):
	if Engine.editor_hint:
		for entity in get_all_entities():
			entity.tile = world_to_map(entity.position)
	else:
		var cursor_position: Vector2 = \
				screen_to_tile(get_global_mouse_position())
		self.cursor_controller.update_cursor(cursor_position, delta)
		for entity in get_all_entities():
			entity._process_entity(self, delta)

func _on_cursor_tile_set(tile_position: Vector2, is_inside_map: bool) -> void:
	emit_signal("cursor_position_set", tile_position,
			get_tile_info_at(tile_position), is_inside_map)

func get_cursor_tile() -> Vector2:
	return cursor_controller.map_position

func get_tile_info_at(tile: Vector2) -> String:
	var tile_info := ""
	if not cursor.visible:
		return tile_info
	var entities := find_all_entities_at(tile)
	for i in entities.size():
		var entity := entities[i] as Entity
		tile_info += entity.get_info() + "\n"
		if i < entities.size() - 1:
			tile_info += "\n"
	return tile_info

func highlight(tiles: Array):
	$Highlight.clear()
	for tile in tiles:
		$Highlight.set_cellv(tile, $Highlight.tile_set.find_tile_by_name("blank"))

func _unhandled_input(event: InputEvent):
	if Engine.editor_hint: return
	if event is InputEventMouseButton and event.pressed:
		match event.button_index:
			BUTTON_LEFT:
				var tile = screen_to_tile(event.position)
				emit_signal("control_requested", "map_tile_clicked", \
							MapTileClickedRequest.new(tile))
			BUTTON_RIGHT:
				emit_signal("control_requested", "cancel", \
							ControlRequest.new())
	elif event.is_action_pressed("interact"):
		emit_signal("control_requested", "interact", \
					ControlRequest.new())

func add_player_at(player: Entity, tile):
	if tile == null:
		tile = get_default_spawn_tile()
	add_entity_at(player, tile)
	move_child(player, 0) # Ensure first place in turn order
	player_path = player.get_path()
	emit_signal("player_added", player)

func remove_player():
	remove_child(get_player())
	player_path = NodePath()

func get_default_spawn_tile() -> Vector2:
	var pos := $DefaultSpawnTile.position as Vector2
	return world_to_map(pos)

func get_player() -> Entity:
	if has_node(player_path):
		return get_node(player_path) as Entity
	else:
		return null

func get_environment() -> Entity:
	return get_node_or_null(environment_path) as Entity

func get_all_entities() -> Array:
	var entities := []
	for child in get_children():
		if child is Entity:
			entities.push_back(child)
	return entities

func add_entity_at(entity: Entity, tile: Vector2):
	entity.tile = tile
	add_child(entity)

func add_vfx_at(vfx: Node2D, tile: Vector2):
	vfx.position = map_to_world(tile) + Tile.center_offset()
	add_vfx(vfx)

func add_vfx(vfx: Node2D):
	vfx.z_index = 1
	vfx.add_to_group(VFX_GROUP)
	if vfx.has_signal("vfx_requested"):
		SignalHelper.safe_connect(vfx, "vfx_requested", self, "add_vfx")
	add_child(vfx)
	if vfx.has_method("_start_vfx"):
		vfx.call_deferred("_start_vfx", self)

func find_entity_at(tile: Vector2) -> Entity:
	for node in get_all_entities():
		var entity := node as Entity
		if entity.tile == tile:
			return entity
	return null

func find_all_entities_at(tile: Vector2) -> Array:
	var entities := []
	for node in get_all_entities():
		var entity := node as Entity
		if entity.tile == tile:
			entities.append(entity)
	return entities

func is_tile_inside(tile: Vector2) -> bool:
	return tile.x >= 0 and tile.x < SIZE and tile.y >= 0 and tile.y < SIZE

func is_blocked_tile(tile: Vector2) -> bool:
	if tile.x < 0 or tile.x >= SIZE or tile.y < 0 or tile.y >= SIZE:
		return true
	var cell = get_cellv(tile)
	var tile_name := tile_set.tile_get_name(cell)
	if tile_name in BLOCKING_TILES:
		return true
	return false

func is_animating() -> bool:
	if is_inside_tree() and not get_tree().get_nodes_in_group(VFX_GROUP).empty():
		return true
	return false

func has_entities_moving() -> bool:
	if is_inside_tree():
		for entity in get_all_entities():
			if entity.is_moving(self):
				return true
	return false

func exit_map(dir_vec: Vector2):
	emit_signal("exited_map", dir_vec)

func win():
	emit_signal("win")

class_name MapInfo extends Reference

var size: int
var smooth: float

func _init(_size: int, _smooth: float):
	self.size = _size
	self.smooth = _smooth

func is_tile_inside_map(tile: Vector2) -> bool:
	return tile.x >= 0 and tile.x < self.size and \
			tile.y >= 0 and tile.y < self.size

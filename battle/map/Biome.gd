tool
class_name Biome extends Resource

enum GeneratorType {
	DENSITY,
	AMOUNT
}

export(GeneratorType) var gen_type = GeneratorType.DENSITY
export(Array, Resource) var entity_generators = [] setget _set_generators

func _init():
	resource_name = "Biome"

func _set_generators(array):
	entity_generators = array
	for i in array.size():
		if array[i] == null:
			match gen_type:
				GeneratorType.DENSITY:
					array[i] = EntityDensityGenerator.new()
				GeneratorType.AMOUNT:
					array[i] = EntityAmountGenerator.new()

class_name Campaign extends Control

export var STARTING_COORDS := Vector2.ZERO
export var TITLE_SCN: PackedScene
export var PLAYER_SCN: PackedScene
export var VICTORY_SCN: PackedScene
export var GAMEOVER_SCN: PackedScene

onready var current_scene_path := NodePath("")

onready var world_map := $WorldMap.compile() as Dictionary
onready var current_coords := -Vector2.ONE # invalid coords
onready var new_coords := STARTING_COORDS
onready var player: Entity = null
onready var player_travel_tile = null
onready var running := false

func _ready():
	$WorldMap.hide()
	randomize()
	SignalHelper.safe_connect(get_tree(), "node_added", VFXPlayer, "_on_node_added")
	call_deferred("go_to_title")

func go_to_title():
	var title_menu := TITLE_SCN.instance()
	title_menu.has_saved_game = $Persist.has_save()
	add_child(title_menu)
	current_scene_path = title_menu.get_path()
	SignalHelper.safe_connect(title_menu, "continue_requested", self, \
								"_on_continue_requested")
	SignalHelper.safe_connect(title_menu, "new_game_requested", self, \
								"_on_new_game_requested")

func _process(_delta):
	if is_campaign_running():
		if new_coords != current_coords:
			if has_node(current_scene_path):
				var battle := get_node(current_scene_path) as Battle
				if player != null:
					player.propagate_call("request_ready")
					battle.get_map().remove_player()
					battle.mark_dirty()
				remove_child(battle)
			if player == null:
				player = PLAYER_SCN.instance()
			current_coords = new_coords
			var battle := update_current_battle()
			battle.get_map().add_player_at(player, player_travel_tile)

func update_current_battle() -> Battle:
	var chunk := world_map[current_coords] as WorldMap.ChunkCache
	var battle := chunk.get_instance()
	battle.request_ready()
	add_child(battle)
	current_scene_path = battle.get_path()
	SignalHelper.safe_connect(
		battle, "battle_won", self, "_on_battle_won"
	)
	SignalHelper.safe_connect(
		battle, "battle_lost", self, "_on_battle_lost"
	)
	SignalHelper.safe_connect(
		battle, "map_exited", self, "_on_map_exited"
	)
	SignalHelper.safe_connect(
		battle, "save_requested", self, "_on_save_requested"
	)
	SignalHelper.safe_connect(
		battle, "game_quit", self, "_on_game_quit"
	)
	return battle

func is_campaign_running() -> bool:
	return running

func _on_new_game_requested():
	get_node(current_scene_path).queue_free()
	running = true

func _on_continue_requested():
	var scene := get_node(current_scene_path)
	scene.queue_free()
	yield(scene, "tree_exited")
	$Persist.load(self)
	running = true

func _on_save_requested():
	$Persist.schedule_save(self)

func _on_battle_won(battle: Battle):
	battle.queue_free()
	var victory := VICTORY_SCN.instance()
	#warning-ignore:return_value_discarded
	victory.connect("tree_exited", self, "go_to_title")
	add_child(victory)
	current_scene_path = victory.get_path()
	reset_campaign()

func _on_battle_lost(battle: Battle):
	remove_child(battle)
	var gameover := GAMEOVER_SCN.instance()
	#warning-ignore:return_value_discarded
	gameover.connect("tree_exited", self, "go_to_title")
	add_child(gameover)
	current_scene_path = gameover.get_path()
	reset_campaign()

func _on_game_quit():
	$Persist.schedule_save(self)
	remove_child(get_node(current_scene_path))
	reset_campaign()
	go_to_title()

func _on_map_exited(dir_vec: Vector2):
	new_coords = current_coords + dir_vec
	if world_map.has(new_coords):
		player_travel_tile = tile_on_other_side(player.tile, dir_vec)
	else:
		new_coords = current_coords

func reset_campaign():
	new_coords = STARTING_COORDS
	current_coords = -Vector2.ONE
	player_travel_tile = null
	player = null
	world_map = $WorldMap.compile()
	running = false

func tile_on_other_side(tile: Vector2, dir: Vector2) -> Vector2:
	return tile - dir * (Map.SIZE - 1)

func _input(_event):
	pass
#	if event.is_action_pressed("debug_save"):
#		$Persist.save(self)
#	elif event.is_action_pressed("debug_load"):
#		get_node(current_scene_path).free()
#		$Persist.load(self)

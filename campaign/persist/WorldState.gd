class_name WorldState extends Resource

export var current_coords := -Vector2.ONE
export var map_states := {}

func _init():
	resource_local_to_scene = true
	current_coords = -Vector2.ONE
	map_states = {}

func save(world_map: Dictionary):
	for coord in world_map:
		var chunk := world_map[coord] as WorldMap.ChunkCache
		var battle := chunk.get_instance() as Battle if chunk != null else null
		if battle != null and battle.is_dirty():
			battle.mark_clean()
			var state := MapState.new()
			state.save(battle)
			if battle.get_map().get_player() != null:
				current_coords = coord
			map_states[coord] = state
		elif battle == null:
			map_states[coord] = null

func load(world_map: Dictionary, battle_scn: PackedScene) -> Vector2:
	for coord in map_states:
		var state := map_states[coord] as MapState
		if state != null:
			var battle := battle_scn.instance() as Battle
			var chunk := WorldMap.ChunkCache.new(null)
			state.load(battle)
			chunk.node = battle
			world_map[coord] = chunk
		else:
			world_map[coord] = null
	return current_coords

class_name CampaignState extends Resource

export var world_state: Resource

func _init():
	world_state = WorldState.new()

func save(campaign: Campaign):
	(world_state as WorldState).save(campaign.world_map)

func load(campaign: Campaign, battle_scn: PackedScene):
	campaign.world_map.clear()
	campaign.current_coords = world_state.load(campaign.world_map, battle_scn)
	campaign.new_coords = campaign.current_coords
	campaign.player_travel_tile = null
	var battle := campaign.update_current_battle()
	campaign.player = battle.get_map().get_player()

class_name MapState extends Resource

export var entity_states := []
export var tiles := {}
export var biome: Resource

func _init():
	resource_local_to_scene = true
	entity_states = []
	tiles = {}

func save(battle: Battle):
	var map := battle.get_map()
	biome = map.biome
	for entity in map.get_all_entities():
		var entity_state := EntityState.new()
		entity_state.save(entity as Entity)
		entity_states.append(entity_state)
	for y in Map.SIZE:
		for x in Map.SIZE:
			tiles[Vector2(x, y)] = {
				type = map.get_cell(x, y),
				x_flip = map.is_cell_x_flipped(x, y),
				y_flip = map.is_cell_y_flipped(x, y),
				transposed = map.is_cell_transposed(x, y),
			}

func load(battle: Battle):
	var map := battle.get_map()
	map.biome = biome
	for item in entity_states:
		var entity_state := item as EntityState
		var entity := entity_state.load_entity()
		map.add_child(entity, true)
		entity.owner = battle
	for y in Map.SIZE:
		for x in Map.SIZE:
			var data := tiles[Vector2(x, y)] as Dictionary
			map.set_cell(x, y, data.type, data.x_flip, data.y_flip, \
						 data.transposed)
	map.update_bitmask_region()

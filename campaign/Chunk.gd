tool class_name Chunk extends TextureRect

export var scene: PackedScene
export var empty_tex: Texture
export var filled_tex: Texture


func _process(_delta):
	if scene != null:
		texture = filled_tex
	else:
		texture = empty_tex

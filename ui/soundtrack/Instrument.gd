class_name Instrument extends Node

const SAMPLE_RATE = 22050.0 # Keep the number of samples to mix low, GDScript is not super fast.
const SAMPLE_MAX_SIZE := 1024

enum WaveForm {
	SINE, PHASER, TRIANGLE, SQUARE_50
}

export(WaveForm) var wave_form
export var pulse_hz := 440.0
export var playing := false
export var volume := 1.0
export var attack := 1.0

var amp := 0.0
var phase = 0.0

class SampleBuffer extends Object:
	var rate: float
	var frames: PoolVector2Array

func _fill_buffer(buffer: SampleBuffer):
	var increment = pulse_hz / buffer.rate
	
	for i in buffer.frames.size():
		if playing:
			amp = min(amp + attack * increment, 1.0)
		else:
			amp = max(amp - attack * increment, 0.0)
		var frame = amp * volume * Vector2.ONE * pulse(phase)
		buffer.frames[i] += frame
		phase = fmod(phase + increment, 1.0)

func pulse(t) -> float:
	t = fmod(t, 1.0)
	match wave_form:
		WaveForm.SINE:
			return sin(t * TAU)
		WaveForm.PHASER:
			return t
		WaveForm.TRIANGLE:
			return t * 2 if t < 0.5 else (1 - t) * 2
		WaveForm.SQUARE_50:
			return 0.0 if t < 0.5 else 1.0
	return 0.0

func play(midi := 69):
	pulse_hz = pow(2.0, (midi - 69.0) / 12.0) * 440.0
	playing = true

func stop():
	playing = false

static func fill_buffer(node: Node, playback: AudioStreamPlayback):
	var to_fill = min(SAMPLE_MAX_SIZE, playback.get_frames_available())
	var derp := []
	derp.resize(to_fill)
	
	var buffer := SampleBuffer.new()
	buffer.rate = 44100.0
	buffer.frames = PoolVector2Array(derp)
	
	for i in to_fill:
		buffer.frames.append(Vector2.ZERO)
	
	for child in node.get_children():
		if child.has_method("_fill_buffer"):
			child._fill_buffer(buffer)
	
	for i in to_fill:
		playback.push_frame(buffer.frames[i])
	
	buffer.free()

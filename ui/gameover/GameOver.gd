class_name GameOver extends Control

func _input(event):
	if event is InputEventMouseButton and event.button_index == 1 and event.is_pressed():
		queue_free()

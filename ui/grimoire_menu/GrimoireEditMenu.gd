extends VBoxContainer

export var GRIMOIRE_CAP := 3

export var grimoire_path := NodePath("")
export var grimoire_title_path := NodePath("")
export var library_path := NodePath("")
export var inspector_name_path := NodePath("")
export var inspector_description_path := NodePath("")

export(Array, PackedScene) var spell_scenes
onready var selected_spells := []

signal grimoire_prepared(spell_list)

# Called when the node enters the scene tree for the first time.
func _ready():
	var list := get_node(library_path) as ItemList
	for spell_scene in spell_scenes:
		var spell := spell_scene.instance() as Spell
		list.add_item(" %s" % spell.spell_name)
		spell.free()

func _process(_delta):
	var grimoire_title := get_node(grimoire_title_path) as Label
	grimoire_title.text = "Grimoire %d/%d" % [selected_spells.size(), GRIMOIRE_CAP]

func _on_library_item_selected(index: int):
	var grimoire := get_node(grimoire_path) as ItemList
	grimoire.unselect_all()
	var spell := spell_scenes[index].instance() as Spell
	fill_spell_inspector(spell.spell_name, spell.description)

func _on_add_button_pressed():
	var library := get_node(library_path) as ItemList
	var selected := library.get_selected_items()
	if selected.size() > 0 and selected_spells.size() < GRIMOIRE_CAP:
		var idx = selected[0]
		if not idx in selected_spells:
			var pos = selected_spells.bsearch(idx)
			var grimoire := get_node(grimoire_path) as ItemList
			var spell := spell_scenes[idx].instance() as Spell
			selected_spells.insert(pos, idx)
			grimoire.add_item(" %s" % spell.spell_name)
			grimoire.move_item(grimoire.get_item_count() - 1, pos)

func _on_grimoire_item_selected(index):
	var library := get_node(library_path) as ItemList
	library.unselect_all()
	var spell := spell_scenes[selected_spells[index]].instance() as Spell
	fill_spell_inspector(spell.spell_name, spell.description)

func _on_remove_button_pressed():
	var grimoire := get_node(grimoire_path) as ItemList
	var selected := grimoire.get_selected_items()
	if selected.size() > 0:
		var idx = selected[0]
		selected_spells.remove(idx)
		grimoire.remove_item(idx)
		fill_spell_inspector("", "")

func fill_spell_inspector(title: String, descr: String):
	var title_label := get_node(inspector_name_path) as Label
	var description_text := get_node(inspector_description_path) as RichTextLabel
	title_label.text = title
	description_text.text = descr

func _on_confrm_button_pressed():
	if selected_spells.size() > 0:
		var spells := []
		for spell_idx in selected_spells:
			spells.append(spell_scenes[spell_idx])
		emit_signal("grimoire_prepared", spells)



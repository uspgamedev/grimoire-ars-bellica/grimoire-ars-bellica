tool
extends TextureRect

export var filled := false

func _process(_delta):
	if self.filled:
		self.modulate = Color.white
	else:
		self.modulate = Color.gray * 0.5

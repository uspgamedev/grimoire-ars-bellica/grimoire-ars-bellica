class_name ConfirmPopup extends CenterContainer

export var label_path: NodePath
export var yes_path: NodePath
export var no_path: NodePath

func _ready():
	hide()

func display(text: String):
	var label := get_node(label_path) as Label
	label.text = text
	show()

func get_yes_button() -> Button:
	return get_node(yes_path) as Button

func get_no_button() -> Button:
	return get_node(no_path) as Button

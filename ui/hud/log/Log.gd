extends TextEdit

func _ready():
	#warning-ignore:return_value_discarded
	get_tree().connect("node_added", self, "connect_message")
	scan(get_tree().get_root())

func scan(node: Node):
	connect_message(node)
	for child in node.get_children():
		scan(child)

func connect_message(node: Node):
	if node.has_signal("message"):
		SignalHelper.safe_connect(node, "message", self, "_on_message")

func _on_message(text: String):
	self.text += "\n\n" + text
	self.scroll_vertical = get_line_count()

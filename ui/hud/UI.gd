class_name UI extends Control

signal control_requested(name, params)
signal exit_requested

export var SPELL_BTN_SCENE: PackedScene
export var GRIMOIRE_PATH: NodePath
export var INSPECTOR_PATH: NodePath
export var HINT_CONTAINER_PATH: NodePath
export var CONFIRM_POPUP: NodePath
export var PRIMARY_ACTION_HINT: NodePath
export var SECONDARY_ACTION_HINT: NodePath
export var SPECIAL_ACTION_HINT: NodePath

onready var spell_list: Node = get_node(GRIMOIRE_PATH)
onready var hint_container: HintContainer = get_node(HINT_CONTAINER_PATH) as HintContainer
onready var show_hint := false

func _ready():
	connect_confirm_popup()
	for spell_button in spell_list.get_children():
		if spell_button is SpellButton:
			#warning-ignore:return_value_discarded
			spell_button.connect("spell_selected", self, "_on_spell_selected")

func connect_confirm_popup():
	var popup := get_node(CONFIRM_POPUP) as ConfirmPopup
	#warning-ignore:return_value_discarded
	popup.get_yes_button().connect("pressed", self, "_on_popup_confirm_clicked")
	#warning-ignore:return_value_discarded
	popup.get_no_button().connect("pressed", self, "_on_popup_cancel_clicked")

func get_confirm_popup() -> ConfirmPopup:
	return get_node(CONFIRM_POPUP) as ConfirmPopup

func load_spells(spells: Array):
	for old_spell in spell_list.get_children():
		if old_spell is SpellButton:
			old_spell.spell_path = NodePath()
	var idx := 0
	for spell in spells:
		if spell is Spell:
			var spell_button := spell_list.get_child(idx*3 + 1) as SpellButton
			spell_button.spell_path = spell.get_path()
			var description = spell.description
			SignalHelper.reconnect(spell_button, "mouse_entered", self, \
								   "show_description", [description])
			var erase_button := spell_list.get_child(idx*3 + 2) as EraseButton
			erase_button.disabled = false
			SignalHelper.reconnect(
				erase_button, "pressed", self, "_on_spell_erased", \
				[spell]
			)
			idx += 1
	for i in range(idx, spell_list.get_child_count()/3):
		var spell_button := spell_list.get_child(idx*3 + 1) as SpellButton
		SignalHelper.safe_disconnect(spell_button, "mouse_entered", \
									 self, "show_description")
		var erase_button := spell_list.get_child(i*3 + 2) as EraseButton
		erase_button.disabled = true

func show_description(info: String):
	#spell_description.text = text
	var inspector := get_node(INSPECTOR_PATH) as RichTextLabel
	inspector.text = info

func set_primary_action_hint(text: String):
	var hint := get_node(PRIMARY_ACTION_HINT) as Label
	hint.text = text

func set_secondary_action_hint(text: String):
	var hint := get_node(SECONDARY_ACTION_HINT) as Label
	hint.text = text

func set_special_action_hint(text: String):
	var hint := get_node(SPECIAL_ACTION_HINT) as Label
	hint.text = text

func _process(_delta):
	show_hint = Input.is_action_pressed("info")

func _on_action_option_selected(action: String):
	emit_signal("control_requested", "action_selected", \
				ActionSelectedRequest.new(action))

func _on_spell_selected(spell: Spell):
	emit_signal("control_requested", "spell_selected", \
				SpellSelectedRequest.new(spell))

func _on_spell_erased(spell: Spell):
	emit_signal("control_requested", "spell_erased", \
				SpellSelectedRequest.new(spell))

func _on_cursor_position_set(_tile_position: Vector2, info: String,
			is_inside_map: bool):
	var inspector := get_node(INSPECTOR_PATH) as RichTextLabel
	if is_inside_map:
		inspector.text = info

func _on_popup_cancel_clicked():
	emit_signal("control_requested", "cancel", ControlRequest.new())

func _on_popup_confirm_clicked():
	emit_signal("control_requested", "confirm", ControlRequest.new())

func _on_exit_pressed():
	emit_signal("exit_requested")

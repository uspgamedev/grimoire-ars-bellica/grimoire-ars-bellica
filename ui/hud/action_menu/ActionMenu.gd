class_name ActionMenu extends PanelContainer

export var ACTION_BTN_SCENE: PackedScene
export var BUTTON_CONTAINER_PATH: NodePath


func add_query_button(text: String) -> Button:
	if has_node(BUTTON_CONTAINER_PATH):
		var button := ACTION_BTN_SCENE.instance() as Button
		button.text = text
		var container := get_node(BUTTON_CONTAINER_PATH) as Container
		container.add_child(button)
		return button
	else:
		return null

func has_queries() -> bool:
	return has_node(BUTTON_CONTAINER_PATH) \
		 and get_node(BUTTON_CONTAINER_PATH).get_child_count() > 1

func clear_query():
	if has_node(BUTTON_CONTAINER_PATH):
		var container := get_node(BUTTON_CONTAINER_PATH) as Container
		for child in container.get_children():
			if child is Button:
				child.queue_free()
